# Questions and Answers Chapter 1

This document holds exercise questions from chapter 1 of the book Low-Level Programming. After each question so follows an answer to the question. The answers are based on reading the book and researching on-line. Every attempt have been made to keep the answers correct but there are likely both big and small mistakes in some of the answers.

## Question 1

It is time to make a first research based on the documentation
[Intel 64 and IA-32 Architectures Software Developer's Manual](https://software.intel.com/en-us/download/intel-64-and-ia-32-architectures-sdm-combined-volumes-1-2a-2b-2c-2d-3a-3b-3c-3d-and-4). Refer to the section 3.4.3 of the first volume to learn
about register `RFLAGS`.

What is the meaning of flags `CF`, `AF`, `ZF`, `OF`, `SF`?

What is the difference between `OF` and `CF`?

## Answer Question 1

Firs note on the `RFLAGS` register is that this register is only available in 64 bit mode and is formed by extending the 32 bit register `EFLAGS` to 64 bits. The lower 32 bits of `RFLAGS` are the same as in `EFLAGS`. We will hence need to study the documentation of `EFLAGS` to find the answer to this question. Now when that's cleared up lets move on to the discussion about the some of the different flags in `RFLAGS`.

`CF` stands for carry flag. This flag is affected by arithmetic operations. Will be set if an arithmetic operation overflows. Will also be set if an arithmetic operation generates a carry or borrow; cleared otherwise. For example so will this bit be set when adding two integers so large that the sum will not fit in the register that shall hold the result.

`AF` stands for auxiliary carry flag. This flag is set if an arithmetic operation generates a carry or borrow out of bit 3 of the result i.e. the most significant bit of the lower nibble. This flag is needed when performing binary-coded decimal (BCD) arithmetic.

`ZF` stands for zero flag. This flag is set if the result of an arithmetic operation is zero; cleared otherwise.

`OF` stands for overflow flag. Set if the integer result is too large a positive number or too small a negative number, excluding the sign-bit, to fit in the destination operand; cleared otherwise

`SF` stands for sign flag.  Set equal to the most-significant bit of the result of an arithmetic operation, which is the sign bit of a signed integer. 0 indicates a positive value and 1 indicates a negative value.

The difference between `CF` and `OF` is that the former is used in unsigned integer arithmetic and the latter in signed integer arithmetic.

## Question 2

What are the key principles of von Neumann architecture?

## Answer Question 2

A computer constructed according to the von Neumann architecture will have a processing unit and a memory unit that are separated by a common bus used to transfer both data and program instructions between the two parts.

The memory unit holds both program instructions and data that is processed during program execution. The memory stores bits that have the value 0 or 1. A byte is commonly 8 bits grouped together and each such byte will have an address to be able to read and write values to the byte location in the memory.

The processing unit holds a arithmetic logic unit (ALU) and a control unit (CU). The ALU is constructed to be able to handle arithmetic operations such as add and subtract of data. The CU handles the communication with the memory and controls the ALU based on the program instructions it reads from the memory.

## Question 3

What are registers?

## Answer Question 3

A processor will work on data fetched from external memory and the result is then stored in the external memory. The communication process goes over a bus between the processor and the external memory. To avoid some of the overhead caused by the bus so are processors usually equipped with memory cells integrated directly into the processor chip. This type of memory are called registers. There are only a few registers and each can hold very little data but the data can be accessed much faster compared to the external memory.

## Question 4

What is the hardware stack?

## Answer Question 4

In computer science, a stack is a container for storing elements. Each new element added to the stack is thought to be placed on top of the existing elements, this is called push. The only way to remove element from the stack is to take out the current top element, this is called pop.

In the book so is the term hardware stack used to describe an emulation of the actual stack that is part of the main memory, there is no separate special stack memory.

The emulation is based around keeping the memory address to the byte that holds the current top of the stack in the `RSP` register. Certain machine instructions such as for example `PUSH` and `POP` relies on the information in the `RSP` register and execution of such commands can cause the content of `RSP` to be updated.

## Question 5

What are the interrupts?

## Answer Question 5

An interrupt is a signal that causes the ordinary sequential code execution to be suspended and instead so will code in what's called an interrupt handler be executed. Once the end of the interrupt handler code have been reached so will the ordinary code start to execute again at the same point where it was before the interrupt occurred.

Examples of sources of interrupts are; hardware interrupts from external devices such as keyboards and network cards, exceptions such as zero division or attempt to execute a privileged instruction in non-privileged mode.

## Question 6

What are the main problems that the modern extensions of von Neuman model are trying to solve?

## Answer Question 6

Latency caused by the need to always use the bus when accessing memory.

Limitations in the interaction with external input/output devices such as keyboards.

Security and stability issues due to all programs being able to access the entire memory and execute all instructions.

## Question 7

What are the main general purpose registers of Intel 64?

## Answer Question 7

In 64-bit mode, there are 16 general purpose registers and they are called; RAX, RBX, RCX, RDX, RDI, RSI, RBP, RSP, R8-R15.

## Question 8

What is the purpose of the stack pointer?

## Answer Question 8

The stack is used during program execution to dynamically store various content needed to keep track of what to execute next and for storing temporal variables used in subroutines.

When jumping from one subroutine to another so must there for example be a record of where the first subroutine lives, to be able to go back once done with the second subroutine, this information is stored on the stack.

The register `RSP` holds a pointer to the top element of the stack, this pointer is in reality an address to memory cells that hold content needed to be stored dynamically during execution. The stack will grow and shrink during program execution and the stack pointer will be updated accordingly.

## Question 9

Can the stack be empty?

## Answer Question 9

Note that this answer is not quite the same as the one provided by the books official answers [list](https://github.com/Apress/low-level-programming/blob/master/questions/answers/009.md), and it is up to the reader how this question really shall be interpreted and then judge the answer.

The stack is considered to be empty prior to starting a program execution and will then grow as things are pushed on to the stack and shrink when things are popped from the stack. It can very well be so that everything from the stack will be popped at some stage and the stack will the be considered empty again.

Executing `POP` when the stack is empty will still return a value but this value will just be a random value that is not related or relevant to the  currently executing program.

## Question 10

Can we count elements in stack?

## Answer Question 11

If we mean the stack in the sense what is stored in `RSP` so will this register hold the address of the top of the stack and this information alone is not enough to count the elements in the stack. So in general is it not so that we can count the elements in the stack.
