# Reading Low-Level Programming

This repository contains code and material related the book Low-Level Programming by Igor Zhirkov.

More information about the book can be found at the books Apress [site](https://www.apress.com/us/book/9781484224021). There is also an accompanying GitHub [site](https://github.com/Apress/low-level-programming) for the book.

Currently working with Ubuntu on Windows subsystem to learn the content of the book.
